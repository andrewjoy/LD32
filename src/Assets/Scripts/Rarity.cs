﻿using UnityEngine;

public enum Rarity
{
    Guaranteed,
    Common,
    Uncommon,
    Rare
}

public static class RarityExtensions
{
    public static float GetChance(this Rarity rarity)
    {
        switch (rarity)
        {
            case Rarity.Guaranteed:
                return 100.0f;
            case Rarity.Common:
                return 0.02f;
            case Rarity.Uncommon:
                return 0.01f;
            case Rarity.Rare:
                return 0.005f;
        }

        return 0.0f;
    }

    public static Color GetColor(this Rarity rarity)
    {
        switch (rarity)
        {
            case Rarity.Common:
                return new Color32(121, 235, 151, 255);
            case Rarity.Uncommon:
                return new Color32(172, 201, 235, 255);
            case Rarity.Rare:
                return new Color32(224, 121, 235, 255);
            case Rarity.Guaranteed:
            default:
                return Color.white;
        }
    }
}
