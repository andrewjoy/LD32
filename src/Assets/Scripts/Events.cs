﻿using System.Collections.Generic;

public sealed class AttendeeOption : Convention.Event.Option
{
    private readonly Attendee.Type type = Attendee.Type.Padawan;
    private readonly int count = 0;

    public AttendeeOption(string name, float chance, Attendee.Type type, int count)
        : base(name, chance)
    {
        this.type = type;
        this.count = count;
    }

    public Attendee.Type Type
    {
        get
        {
            return this.type;
        }
    }

    public int Count
    {
        get
        {
            return this.count;
        }
    }

    public override string Description
    {
        get
        {
            return this.count + " " + this.type.GetReadableString();
        }
    }

    protected override void ProcessApply(Convention target)
    {
        target.SpawnAttendees(this.type, this.count);
    }
}

public sealed class GrowthOption : Convention.Event.Option
{
    private readonly Attendee.Type type = Attendee.Type.Padawan;
    private readonly int count = 0;

    public GrowthOption(string name, float chance, Attendee.Type type, int count)
        : base(name, chance)
    {
        this.type = type;
        this.count = count;
    }

    public Attendee.Type Type
    {
        get
        {
            return this.type;
        }
    }

    public int Count
    {
        get
        {
            return this.count;
        }
    }

    public override string Description
    {
        get
        {
            return string.Concat(this.count, " ", this.type.GetReadableString(), " Every 10sec");
        }
    }

    protected override void ProcessApply(Convention target)
    {
        target.AddAttendeeGrowth(this.type, this.count);
    }
}

public sealed class HealOption : Convention.Event.Option
{
    private readonly int count = 0;

    public HealOption(string name, float chance, int count)
        : base(name, chance)
    {
        this.count = count;
    }

    public int Count
    {
        get
        {
            return this.count;
        }
    }

    public override string Description
    {
        get
        {
            return this.count + " Health";
        }
    }

    protected override void ProcessApply(Convention target)
    {
        target.Health += count;
    }
}
