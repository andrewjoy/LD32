﻿using UnityEngine;

public sealed class Billboard : MonoBehaviour
{
    private void LateUpdate()
    {
        this.transform.rotation = Quaternion.LookRotation(Camera.main.transform.position - this.transform.position);
    }
}
