﻿using UnityEngine;

public enum Alignment
{
    Good,
    Bad
}

public static class AlignmentExtensions
{
    public static Color GetColor(this Alignment relationship)
    {
        switch (relationship)
        {
            case Alignment.Good:
                return new Color(0.0f, 0.5f, 1.0f);
            case Alignment.Bad:
                return Color.red;
        }

        return Color.white;
    }
}
