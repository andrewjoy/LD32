﻿using System.Text;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public sealed class ConventionUI : MonoBehaviour
{
    private const int MAX_LIST_VISIBLE = 5;
    private const int MOVE_COST_PER_METER = 30;

    [SerializeField]
    private Convention target = null;

    [SerializeField]
    private RectTransform eventInformationPanel = null;

    [SerializeField]
    private Image eventInformationBackground = null;

    [SerializeField]
    private Text eventInformationTitleText = null;

    [SerializeField]
    private RectTransform eventOptionsPanel = null;

    [SerializeField]
    private Text eventInformationOption1Text = null;

    [SerializeField]
    private MultitextButton eventInformationOption1Button = null;

    [SerializeField]
    private Text eventInformationOption2Text = null;

    [SerializeField]
    private MultitextButton eventInformationOption2Button = null;

    [SerializeField]
    private RectTransform successPanel = null;

    [SerializeField]
    private RectTransform failurePanel = null;

    private float lockedTimeRemaining = 0.0f;

    public void ReloadLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void DiscardSelectedEvent()
    {
        Convention.Event selectedEvent = this.GetSelectedEvent();
        if (selectedEvent != null)
        {
            this.target.RemoveEvent(selectedEvent);

            selectedEvent = null;

            this.RefreshUI();
        }
    }

    public void ApplySelectedEventOption1()
    {
        Convention.Event selectedEvent = this.GetSelectedEvent();
        if (selectedEvent != null)
        {
            bool success = selectedEvent.ApplyOption1(this.target);

            this.lockedTimeRemaining = 1.0f;

            this.eventOptionsPanel.gameObject.SetActive(false);
            this.successPanel.gameObject.SetActive(success);
            this.failurePanel.gameObject.SetActive(!success);

            this.DiscardSelectedEvent();
        }
    }

    public void ApplySelectedEventOption2()
    {
        Convention.Event selectedEvent = this.GetSelectedEvent();
        if (selectedEvent != null)
        {
            bool success = selectedEvent.ApplyOption2(this.target);

            this.lockedTimeRemaining = 1.0f;

            this.eventOptionsPanel.gameObject.SetActive(false);
            this.successPanel.gameObject.SetActive(success);
            this.failurePanel.gameObject.SetActive(!success);

            this.DiscardSelectedEvent();
        }
    }

    public void RefreshUI()
    {
        RectTransform eventTransform = (RectTransform)this.eventInformationPanel.transform;

        if (this.lockedTimeRemaining <= 0.0f)
        {
            this.eventOptionsPanel.gameObject.SetActive(true);
            this.successPanel.gameObject.SetActive(false);
            this.failurePanel.gameObject.SetActive(false);

            Convention.Event selectedEvent = this.GetSelectedEvent();
            if (selectedEvent != null)
            {
                eventTransform.anchoredPosition = Vector2.Lerp(eventTransform.anchoredPosition, new Vector2(0.0f, -100.0f), Time.deltaTime * 5.0f);

                this.eventInformationBackground.color = selectedEvent.Rarity.GetColor();

                eventInformationTitleText.text = selectedEvent.Title;

                eventInformationOption1Text.text = string.Concat(
                    selectedEvent.Option1.Chance * 100.0f,
                    "% chance for:\n",
                    selectedEvent.Option1.Description);
                eventInformationOption1Button.Body.text = selectedEvent.Option1.Name;

                eventInformationOption2Text.text = string.Concat(
                    selectedEvent.Option2.Chance * 100.0f,
                    "% chance for:\n",
                    selectedEvent.Option2.Description);
                eventInformationOption2Button.Body.text = selectedEvent.Option2.Name;
            }
            else
            {
                eventTransform.anchoredPosition = Vector2.Lerp(eventTransform.anchoredPosition, new Vector2(0.0f, 200.0f), Time.deltaTime * 5.0f);
            }
        }
        else
        {
            eventTransform.anchoredPosition = Vector2.Lerp(eventTransform.anchoredPosition, new Vector2(0.0f, -100.0f), Time.deltaTime * 5.0f);

            this.lockedTimeRemaining -= Time.deltaTime;
        }
    }

    private Convention.Event GetSelectedEvent()
    {
        if (this.target != null)
        {
            IEnumerator<Convention.Event> conventionEvents = this.target.Events.GetEnumerator();

            if (conventionEvents.MoveNext())
            {
                return conventionEvents.Current;
            }
        }

        return null;
    }

    private void Update()
    {
        this.RefreshUI();
    }
}
