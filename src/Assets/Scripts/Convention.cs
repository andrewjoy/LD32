﻿using System.Text;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public sealed class Convention : MonoBehaviour
{
    private const int HEALTH_MAX = 100;

    private const float SPAWN_VARIANCE = 0.5f;

    private const float INTERVAL_EVENT = 1.0f;
    private const float INTERVAL_GROWTH = 10.0f;

    private readonly List<Event> events = new List<Event>();

    private readonly Dictionary<Attendee.Type, int> attendeeGrowth = new Dictionary<Attendee.Type, int>()
        {
            { Attendee.Type.Padawan, 1 }
        };

    [SerializeField]
    private new string name = null;

    [SerializeField]
    private int health = HEALTH_MAX;

    [SerializeField]
    private RectTransform healthbarPanel = null;

    [SerializeField]
    private MultitextButton endGameButton = null;

    [SerializeField]
    private Alignment alignment = Alignment.Good;

    [SerializeField]
    private Vector2 spawnOffset = Vector2.zero;

    [SerializeField]
    private Vector2 spawnRotation = Vector2.zero;

    [SerializeField]
    private string spawnLayer = "";

    private float eventIntervalOffset = 0.0f;

    public IEnumerable<Event> Events
    {
        get
        {
            return this.events;
        }
    }

    public IEnumerable<KeyValuePair<Attendee.Type, int>> AttendeeGrowth
    {
        get
        {
            return this.attendeeGrowth;
        }
    }

    public string Name
    {
        get
        {
            return this.name;
        }
    }

    public int Health
    {
        get
        {
            return this.health;
        }

        set
        {
            if (this.health > 0)
            {
                this.health = value;

                if (this.health > HEALTH_MAX)
                {
                    this.health = HEALTH_MAX;
                }

                if (this.health <= 0)
                {
                    this.endGameButton.gameObject.SetActive(true);

                    this.endGameButton.Body.text = string.Concat(
                        "You ",
                        (this.Alignment == Alignment.Good ? "Lose" : "Win"),
                        "! Play Again?");
                }
            }
        }
    }

    public Alignment Alignment
    {
        get
        {
            return this.alignment;
        }
    }

    public void AddEvent(Event newEvent)
    {
        if (!this.events.Contains(newEvent))
        {
            this.events.Add(newEvent);
        }
    }

    public void RemoveEvent(Event oldEvent)
    {
        this.events.Remove(oldEvent);
    }

    public int GetAttendeeGrowth(Attendee.Type attendeeType)
    {
        int currentGrowth = 0;

        this.attendeeGrowth.TryGetValue(attendeeType, out currentGrowth);

        return currentGrowth;
    }

    public void AddAttendeeGrowth(Attendee.Type attendeeType, int count)
    {
        int currentGrowth = this.GetAttendeeGrowth(attendeeType);

        if (currentGrowth + count <= 0)
        {
            this.attendeeGrowth.Remove(attendeeType);
        }
        else
        {
            this.attendeeGrowth[attendeeType] = currentGrowth + count;
        }
    }

    public void SpawnAttendee(Attendee.Type attendeeType)
    {
        this.SpawnAttendees(attendeeType, 1);
    }

    public void SpawnAttendees(Attendee.Type attendeeType, int count)
    {
        Attendee attendePrefab = attendeeType.GetPrefab();

        for (int i = 0; i < count; ++i)
        {
            ((Attendee)Instantiate(
               attendePrefab,
               this.transform.position + (Vector3)this.spawnOffset + new Vector3(Random.Range(-SPAWN_VARIANCE, SPAWN_VARIANCE), Random.Range(0.0f, SPAWN_VARIANCE), 0.0f),
               Quaternion.Euler(this.spawnRotation.x, this.spawnRotation.y, 0.0f))).gameObject.layer = LayerMask.NameToLayer(this.spawnLayer);
        }
    }

    private void Start()
    {
        this.eventIntervalOffset = UnityEngine.Random.value;
    }

    private void Update()
    {
        if (this.health > 0)
        {
            if ((this.eventIntervalOffset + Time.timeSinceLevelLoad) % INTERVAL_EVENT < Time.deltaTime)
            {
                float eventChance = 1.0f;

                eventChance *= INTERVAL_EVENT;

                Event[] newEvents = Event.RetrieveRandomlyFromPool(eventChance);
                for (int i = 0; i < newEvents.Length; ++i)
                {
                    this.AddEvent(newEvents[i]);
                }
            }

            if (Time.timeSinceLevelLoad % INTERVAL_GROWTH < Time.deltaTime)
            {
                foreach (KeyValuePair<Attendee.Type, int> attendeeGrowth in this.attendeeGrowth)
                {
                    for (int i = 0; i < attendeeGrowth.Value; ++i)
                    {
                        this.SpawnAttendee(attendeeGrowth.Key);
                    }
                }
            }

            this.healthbarPanel.anchorMax = new Vector2(Mathf.Clamp01(this.health / (float)HEALTH_MAX), 1.0f);

            if (this.alignment == Alignment.Bad)
            {
                if (this.events.Count > 0)
                {
                    if (Random.value >= 0.5f)
                    {
                        this.events[0].ApplyOption1(this);
                    }
                    else
                    {
                        this.events[0].ApplyOption2(this);
                    }

                    this.events.RemoveAt(0);
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position + (Vector3)this.spawnOffset, 0.1f);
    }

    public sealed class Event
    {
        private static readonly List<Event> pool = new List<Event>();

        public static IEnumerable<Event> Pool
        {
            get
            {
                return pool;
            }
        }

        public static void AddToPool(Event newEvent)
        {
            pool.Add(newEvent);
        }

        public static Event[] RetrieveRandomlyFromPool(float chanceModifier)
        {
            List<Event> returnValue = new List<Event>();

            for (int i = 0; i < pool.Count; ++i)
            {
                float chance = UnityEngine.Random.value;

                if (chance < pool[i].Rarity.GetChance() * chanceModifier)
                {
                    returnValue.Add(pool[i]);
                }
            }

            return returnValue.ToArray();
        }

        private readonly Option option1 = null;
        private readonly Option option2 = null;

        private readonly Rarity rarity = Rarity.Uncommon;

        private readonly string title = null;

        public Option Option1
        {
            get
            {
                return this.option1;
            }
        }

        public Option Option2
        {
            get
            {
                return this.option2;
            }
        }

        public Rarity Rarity
        {
            get
            {
                return this.rarity;
            }
        }

        public string Title
        {
            get
            {
                return this.title;
            }
        }

        public Event(Rarity rarity, string title, Option option1, Option option2)
        {
            this.rarity = rarity;

            this.title = title;

            this.option1 = option1;
            this.option2 = option2;
        }

        public bool ApplyOption1(Convention target)
        {
            return this.option1.Apply(target);
        }

        public bool ApplyOption2(Convention target)
        {
            return this.option2.Apply(target);
        }

        public abstract class Option
        {
            private string name = null;
            private float chance = 1.0f;

            public Option(string name, float chance)
            {
                this.name = name;
                this.chance = chance;
            }

            public string Name
            {
                get
                {
                    return this.name;
                }
            }

            public float Chance
            {
                get
                {
                    return this.chance;
                }
            }

            public abstract string Description { get; }

            public bool Apply(Convention target)
            {
                if (UnityEngine.Random.value <= this.chance)
                {
                    this.ProcessApply(target);

                    return true;
                }

                return false;
            }

            protected abstract void ProcessApply(Convention target);
        }
    }
}
