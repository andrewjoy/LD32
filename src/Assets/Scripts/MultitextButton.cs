﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public sealed class MultitextButton : MonoBehaviour
{
    private Button button = null;

    [SerializeField]
    private Text prefix = null;

    [SerializeField]
    private Text body = null;

    [SerializeField]
    private Text suffix = null;

    public Button Button
    {
        get
        {
            return this.button;
        }
    }

    public Text Prefix
    {
        get
        {
            return this.prefix;
        }
    }

    public Text Body
    {
        get
        {
            return this.body;
        }
    }

    public Text Suffix
    {
        get
        {
            return this.suffix;
        }
    }

    private void Awake()
    {
        this.button = this.gameObject.GetComponent<Button>();
    }
}
