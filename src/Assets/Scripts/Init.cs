﻿using UnityEngine;

public sealed class Init : MonoBehaviour
{
    private void Awake()
    {
        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Free Ticket Day",
            new AttendeeOption("Only the Pretty Ones", 0.8f, Attendee.Type.GundamCosplayer, 1),
            new AttendeeOption("All Allowed", 1.0f, Attendee.Type.Padawan, 10)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Visit from Maile Flanagan",
            new AttendeeOption("Naruto Trivia Night", 0.7f, Attendee.Type.WhiteSamurai, 6),
            new AttendeeOption("Standard Panel", 1.0f, Attendee.Type.WhiteSamurai, 3)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Visit from Nathan Fillion",
            new HealOption("Run as Fundraiser", 1.0f, 10),
            new AttendeeOption("Standard Panel", 1.0f, Attendee.Type.Browncoat, 3)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Visit from Wil Wheaton",
            new AttendeeOption("Imprompty D&D Session", 0.85f, Attendee.Type.Trekkie, 5),
            new AttendeeOption("Standard Panel", 1.0f, Attendee.Type.Trekkie, 3)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Rare,
            "Engineer Attempts Cosplay",
            new AttendeeOption("Daddy Daughter Day", 0.75f, Attendee.Type.GundamCosplayer, 2),
            new AttendeeOption("Ooo, Shiny", 1.0f, Attendee.Type.GundamCosplayer, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Hire Marvel Marketing Exec",
            new AttendeeOption("To the Front Lines!", 1.0f, Attendee.Type.Padawan, 1),
            new HealOption("Worth the Lambourghini", 1.0f, 10)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Unisex Toilets",
            new HealOption("No Doors", 1.0f, 10),
            new GrowthOption("As Advertised", 1.0f, Attendee.Type.Padawan, 2)));

        Convention.Event.AddToPool( new Convention.Event(
            Rarity.Uncommon,
            "Communal Hug Pillow",
            new HealOption("Ick", 1.0f, 10),
            new GrowthOption("Just a Little Damp", 1.0f, Attendee.Type.WhiteSamurai, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Life-Size Cardboard Jewel Staite",
            new HealOption("To Ebay!", 1.0f, 10),
            new GrowthOption("Lifelike", 1.0f, Attendee.Type.Browncoat, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Rumours of Nimoy Sightings",
            new HealOption("Siri, Call 555-2368", 1.0f, 10),
            new GrowthOption("Spooky", 1.0f, Attendee.Type.Trekkie, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Rare,
            "Free Oil Changes",
            new HealOption("Oil Isn't Cheap", 1.0f, 50),
            new GrowthOption("Not a Squeek", 1.0f, Attendee.Type.GundamCosplayer, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Uncommon,
            "Ding!",
            new HealOption("I Want My Money Back", 0.6f, 40),
            new HealOption("Gratz", 1.0f, 20)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Rare,
            "Date a Celebrity",
            new HealOption("Pre-Nup", 1.0f, 30),
            new HealOption("Messy Breakup", 0.6f, 50)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Rare,
            "Ban George Clooney",
            new HealOption("No-One Likes Him", 1.0f, -30),
            new HealOption("Don't Be Silly", 1.0f, 30)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Katana Outreach Program",
            new AttendeeOption("Underfund It", 1.0f, Attendee.Type.WhiteSamurai, 3),
            new GrowthOption("I Could Cut Myself...", 0.75f, Attendee.Type.WhiteSamurai, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Discount Brown Coats",
            new AttendeeOption("Cheap Imports", 1.0f, Attendee.Type.Browncoat, 3),
            new GrowthOption("Keeps the Rain Off", 0.75f, Attendee.Type.Browncoat, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Write Technobabble for Dummies",
            new AttendeeOption("Misspell Dummies", 1.0f, Attendee.Type.Trekkie, 3),
            new GrowthOption("From an Expert", 0.75f, Attendee.Type.Trekkie, 1)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Recite Euphoric Quotes",
            new AttendeeOption("From Reddit", 0.2f, Attendee.Type.WhiteSamurai, 10),
            new AttendeeOption("Google First", 0.8f, Attendee.Type.WhiteSamurai, 4)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Fake Firefly Episode 15",
            new AttendeeOption("Star as Yourself", 0.2f, Attendee.Type.Browncoat, 10),
            new AttendeeOption("Summer Glau Look-a-Like", 0.8f, Attendee.Type.Browncoat, 4)));

        Convention.Event.AddToPool(new Convention.Event(
            Rarity.Common,
            "Kiss Kirk",
            new AttendeeOption("Yourself", 0.2f, Attendee.Type.Trekkie, 10),
            new AttendeeOption("Someone Else", 0.8f, Attendee.Type.Trekkie, 4)));

        MonoBehaviour.Destroy(this);
    }
}
