﻿using System;
using System.Text;

using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public sealed class Attendee : MonoBehaviour
{
    private const float FORCE = 100.0f;

    [SerializeField]
    private int health = 0;

    public enum Type
    {
        Padawan,
        WhiteSamurai,
        Browncoat,
        Trekkie,
        GundamCosplayer,
        PartTimeLycanthrope
    }

    public int Health
    {
        get
        {
            return this.health;
        }

        set
        {
            if (health > 0)
            {
                this.health = value;

                if (this.health <= 0)
                {
                    Collider2D[] childColliders = this.gameObject.GetComponentsInChildren<Collider2D>();
                    for (int i = 0; i < childColliders.Length; ++i)
                    {
                        childColliders[i].enabled = false;
                    }

                    this.Rigidbody2D.fixedAngle = false;

                    this.Rigidbody2D.AddTorque(FORCE * Mathf.Sign(this.transform.rotation.eulerAngles.y));

                    GameObject.Destroy(this.gameObject, 3);
                }
            }
        }
    }

    public Rigidbody2D Rigidbody2D { get; private set; }

    private void Start()
    {
        this.Rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        this.Rigidbody2D.AddForce(this.transform.rotation * new Vector2(FORCE * Time.deltaTime, 0.0f));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.health > 0)
        {
            Attendee collisionAttendee = collision.gameObject.GetComponent<Attendee>();
            if (collisionAttendee != null)
            {
                int damage = Mathf.Min(collisionAttendee.health, this.health);

                collisionAttendee.Health -= damage;
                this.Health -= damage;
            }
            else
            {
                Convention collisionConvention = collision.gameObject.GetComponent<Convention>();
                if (collisionConvention != null)
                {
                    collisionConvention.Health -= this.Health;
                    this.Health = 0;
                }
                else
                {
                    this.Rigidbody2D.AddForce(new Vector2(0.0f, FORCE));
                }
            }
        }
    }
}

public static class AttendeeExtensions
{
    public static string GetReadableString(this Attendee.Type attendeeType)
    {
        string unformattedName = attendeeType.ToString();

        StringBuilder formattedName = new StringBuilder();

        if (unformattedName.Length > 0)
        {
            formattedName.Append("<color=#00ff00ff>");

            formattedName.Append(unformattedName[0]);

            for (int i = 1; i < unformattedName.Length; ++i)
            {
                if (char.IsUpper(unformattedName[i]))
                {
                    formattedName.Append(' ');
                }

                formattedName.Append(unformattedName[i]);
            }

            formattedName.Append("</color>");
        }

        return formattedName.ToString();
    }

    public static Attendee GetPrefab(this Attendee.Type attendeeType)
    {
        return ((GameObject)Resources.Load("Attendee_" + attendeeType.ToString())).GetComponent<Attendee>();
    }
}
